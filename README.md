# SDL 2 DOOM

## Screenshots 

<br><br>
<center> Ultimate DOOM </center>
<p align="center">
  <img src="https://github.com/AlexOberhofer/sdl2_doom/raw/master/docs/ultimatedoom.PNG" alt="Ultimate Doom"/> <br>
</p><br><br><br>

<center> DOOM II </center>
<p align="center">
    <img src="https://github.com/AlexOberhofer/sdl2_doom/raw/master/docs/doom2.PNG" alt="Doom II"/>
</p><br><br><br>

<center> TNT Evilution </center>
<p align="center">
    <img src="https://github.com/AlexOberhofer/sdl2_doom/raw/master/docs/tnt.PNG" alt="Doom II"/>
</p><br><br><br>

<center> The Plutonia Experiment </center>
<p align="center">
    <img src="https://github.com/AlexOberhofer/sdl2_doom/raw/master/docs/plutonia.PNG" alt="Doom II"/>
</p><br><br><br>

## Overview

This is a source port of the linuxdoom 1.10 engine, developed mostly as an educational tool for myself. Anyone 
interested in hacking on this project is more than welcome to submit a pull request. This project is currently
in very early development. See the todo file in /docs/ to see the list of additions I would like to make to
this engine.
 
## Future

I hope to work on this project again soon. Hopefully a windows port will follow. Please feel free to hack on this codebase and submit any issues or pull requests.

## License 

This project is licensed under the GNU GPL v2.0 License - see the [LICENSE.md](LICENSE.md) file for details
